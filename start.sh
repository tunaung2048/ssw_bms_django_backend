

# Run server
pipenv shell
pipenv install
cd server
python manage.py collectstatic
# start gunicorn
# python manage.py runserver --settings settings.django_settings.prod