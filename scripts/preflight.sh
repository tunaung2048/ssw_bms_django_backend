#! /bin/bash


echo "Preflight setup..."
pwd
ls | grep "venv/prod" > /dev/null 2>&1
if [ $? == 0 ]
then
  echo "Python vevn available"
  source venv/bin/activate
else
  virtualenv -p python3 venv
  source venv/bin/activate
  pip install -r req.txt
  echo "Python venv has been installed with req.txt"
fi
# cd ~
# mkdir Preflights