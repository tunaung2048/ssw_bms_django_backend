# settings/dev.py
from ssw_bms.settings.base import *  # noqa

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '%t%20*q_0v^t1f!ffk29$a*fzq#(+qhg0(4t+%dee3$i3k_nj1'

DEBUG = True


""" https://github.com/adamchainz/django-cors-headers
    CORS_ALLOWED_ORIGINS, CORS_ALLOWED_ORIGIN_REGEXES, CORS_ALLOW_ALL_ORIGINS
"""
CORS_ALLOWED_ORIGINS = [
    'http://127.0.0.1:8000',
    'http://localhost:8000',
]
# CORS_ALLOWED_ORIGIN_REGEXES = [
#     r"^https://\w+\.myallowedorigin\.com$",
# ]
CORS_ALLOW_ALL_ORIGINS = True


# INSTALLED_APPS += ['debug_toolbar',]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'mydb2',
        'USER': 'mydb2_user',
        'PASSWORD': 'abc123',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

# TIME_ZONE = 'Asia/Rangoon'


CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
    }
}
