# prod.py
from os import path
from ssw_bms.settings.django_settings.base import *  # noqa
from ssw_bms.settings.django_settings.base import BASE_DIR  # noqa
from commons.utils.env_utils import env


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env('SECRET_KEY')

DEBUG = env('DEBUG')

ALLOWED_HOSTS = [
    'http://localhost:' + env('PORT'),
    'http://127.0.0.1:' + env('PORT'),
    'https://backend.' + env('HOSTNAME'),
    'http://backend.' + env('HOSTNAME'),
]


""" https://github.com/adamchainz/django-cors-headers
    CORS_ALLOWED_ORIGINS, CORS_ALLOWED_ORIGIN_REGEXES, CORS_ALLOW_ALL_ORIGINS
"""
CORS_ALLOWED_ORIGINS = [
    # https://backend.mydomain.com
    'https://' + env('SUBDOMAIN') + env('HOSTNAME'),
    'http://localhost',
    'http://127.0.0.1',
]
# Only for when there are many subdomains
CORS_ALLOWED_ORIGIN_REGEXES = [
    # r"^https://\w+\." + env("HOSTNAME") + "\.com$",
    # f'r"^https://\w+\.{env("HOSTNAME")}\.com$"',
]
CORS_ALLOW_ALL_ORIGINS = env('CORS_ALLOW_ALL_ORIGINS')


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': env('DATABASE_NAME'),
        'USER': env('DATABASE_USERNAME'),
        'PASSWORD': env('DATABASE_PASSWORD'),
        'HOST': env('DATABASE_HOST'),
        'PORT': env('DATABASE_PORT'),
    }
}

TIME_ZONE = 'Asia/Rangoon'

STATICFILES_DIRS = [
    # ~/media/backend.luduelectric.com/staticfiles
    path.join(path.dirname(BASE_DIR), 'assets'),
    # ...
]

STATIC_URL = '/static/'
# ~/media/mysite.com/staticfiles
STATIC_ROOT = path.abspath(env('STATIC_ROOT', '~/media/mediafiles/'))

MEDIA_URL = '/media/'
# ~/media/mysite.com/staticfiles
MEDIA_ROOT = path.abspath(env('MEDIA_ROOT', '~/static/staticfiles/'))


CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
        'LOCATION': env('FILE_BASED_CACHE_LOCATION'),
    }
}

from ssw_bms.settings.simplejwt_settings.prod import *  # noqa
