# server/ssw_bms/settings/django/base.py
from os import path

# BASE_DIR = prj_root/server/
BASE_DIR = path.join(path.dirname(path.abspath(__file__)), '../../../')

# BASE_DIR = path.dirname(path.dirname(
#     path.dirname(path.dirname(path.abspath(__file__)))))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.2/howto/deployment/checklist/

# Override in dev.py, prod.py, etc.,
SECRET_KEY = '%t%20*q_0v^t1f!ffk29$a*fzq#(+qhg0(4t+%dee3$i3k_nj1'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []

""" https://github.com/adamchainz/django-cors-headers
    CORS_ALLOWED_ORIGINS, CORS_ALLOWED_ORIGIN_REGEXES, CORS_ALLOW_ALL_ORIGINS
"""
# CORS_ALLOWED_ORIGINS = [
#     "https://example.com",
#     "https://sub.example.com",
#     "http://localhost:8080",
#     "http://127.0.0.1:9000"
# ]
# CORS_ALLOWED_ORIGIN_REGEXES = [
#     r"^https://\w+\.example\.com$",
# ]
# CORS_ALLOW_ALL_ORIGINS = False

# CORS_URLS_REGEX = r'^/api/.*$'
# CORS_ALLOW_METHODS = [
#     'DELETE',
#     'GET',
#     'OPTIONS',
#     'PATCH',
#     'POST',
#     'PUT',
# ]


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Third party
    'rest_framework',
    'mptt',
    'corsheaders',

    # Custom
    'accounts.apps.AccountsConfig',
    'products.apps.ProductsConfig',
    'inventory.apps.InventoryConfig',
    'locations.apps.LocationsConfig',
]

# use Custom User Model
# ** DO NOT CHANGE THIS AFTER PROJECT START
AUTH_USER_MODEL = 'accounts.User'

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',

    # corsheaders middleware ** orders important
    'corsheaders.middleware.CorsMiddleware',

    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.locale.LocaleMiddleware',
]

# DJANGO CACHING
# CACHES = {
# 'default': {
#     'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
#     'LOCATION': 'unique-snowflake',
# }
# 'default': {
#     'BACKEND': 'django.core.cache.backends.filebased.FileBasedCache',
#     'LOCATION': '/var/tmp/django_cache',
# }
# 'default': {
#     'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
#     'LOCATION': 'my_cache_table',
# }
# }

ROOT_URLCONF = 'ssw_bms.urls'
# APPEND_SLASH = True # Default is True

TEMPLATES_DIR = path.join(BASE_DIR, 'templates')
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [TEMPLATES_DIR],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'ssw_bms.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.2/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Custom User Model
# ** DO NOT CHANGE THIS AFTER PROJECT START
AUTH_USER_MODEL = 'accounts.User'
# Custom LOGIN URL
# LOGIN_URL = '/account/login'

# Password validation
# https://docs.djangoproject.com/en/2.2/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',  # noqa
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',  # noqa
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.2/howto/static-files/
# STATIC_URL = '/static/'
# STATIC_ROOT = os.path.join(BASE_DIR, 'static')
# STATICFILES_DIRS = [
#     os.path.join(PROJECT_BASE_DIR, '../assets/static'),
# ]


STATICFILES_DIRS = [
    # prj_root/assets/staticfiles
    path.join(path.dirname(BASE_DIR), 'assets'),
    # ...
]

STATIC_URL = '/static/'
STATIC_ROOT = path.join(BASE_DIR, 'ssw_bms/static')

MEDIA_URL = '/media/'
MEDIA_ROOT = path.join(BASE_DIR, 'ssw_bms/media')

# Loads others base settings
# DJANGO REST FRAMEWORK
from ssw_bms.settings.drf_settings.base import *  # noqa
# DJANGO MPTT Modified Preorderd Tree Traversal
from ssw_bms.settings.mptt_settings.base import *  # noqa
# SIMPLEJWT
from ssw_bms.settings.simplejwt_settings.base import *  # noqa
