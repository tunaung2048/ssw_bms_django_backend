# Templates Notes

## Base Template

### Blocks

```jinja
    {% title %}
    {% endblock title %}

    {% style %}
    {% endblock style %}

    {% navbar %}
    {% endblock navbar %}

    {% pagetitle %}
    {% endblock pagetitle %}

    {% content %}
    {% endblock content %}

    {% footer %}
    {% endblock footer %}

    {% javascript %}
    {% endblock javascript %}
```
