# Generated by Django 2.2 on 2020-12-09 14:19

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='brand',
            name='created_at',
            field=models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Created at'),
        ),
        migrations.AddField(
            model_name='brand',
            name='deleted_at',
            field=models.DateTimeField(blank=True, default=None, null=True, verbose_name='Deleted at'),
        ),
        migrations.AddField(
            model_name='brand',
            name='modified_at',
            field=models.DateTimeField(blank=True, default=None, null=True, verbose_name='Modified at'),
        ),
        migrations.AddField(
            model_name='category',
            name='created_at',
            field=models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Created at'),
        ),
        migrations.AddField(
            model_name='category',
            name='deleted_at',
            field=models.DateTimeField(blank=True, default=None, null=True, verbose_name='Deleted at'),
        ),
        migrations.AddField(
            model_name='category',
            name='modified_at',
            field=models.DateTimeField(blank=True, default=None, null=True, verbose_name='Modified at'),
        ),
        migrations.AddField(
            model_name='mapproductbrand',
            name='created_at',
            field=models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Created at'),
        ),
        migrations.AddField(
            model_name='mapproductbrand',
            name='deleted_at',
            field=models.DateTimeField(blank=True, default=None, null=True, verbose_name='Deleted at'),
        ),
        migrations.AddField(
            model_name='mapproductbrand',
            name='modified_at',
            field=models.DateTimeField(blank=True, default=None, null=True, verbose_name='Modified at'),
        ),
        migrations.AddField(
            model_name='mapproductcategory',
            name='created_at',
            field=models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Created at'),
        ),
        migrations.AddField(
            model_name='mapproductcategory',
            name='deleted_at',
            field=models.DateTimeField(blank=True, default=None, null=True, verbose_name='Deleted at'),
        ),
        migrations.AddField(
            model_name='mapproductcategory',
            name='modified_at',
            field=models.DateTimeField(blank=True, default=None, null=True, verbose_name='Modified at'),
        ),
        migrations.AddField(
            model_name='mapproductuom',
            name='created_at',
            field=models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Created at'),
        ),
        migrations.AddField(
            model_name='mapproductuom',
            name='deleted_at',
            field=models.DateTimeField(blank=True, default=None, null=True, verbose_name='Deleted at'),
        ),
        migrations.AddField(
            model_name='mapproductuom',
            name='modified_at',
            field=models.DateTimeField(blank=True, default=None, null=True, verbose_name='Modified at'),
        ),
        migrations.AddField(
            model_name='product',
            name='created_at',
            field=models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Created at'),
        ),
        migrations.AddField(
            model_name='product',
            name='deleted_at',
            field=models.DateTimeField(blank=True, default=None, null=True, verbose_name='Deleted at'),
        ),
        migrations.AddField(
            model_name='product',
            name='modified_at',
            field=models.DateTimeField(blank=True, default=None, null=True, verbose_name='Modified at'),
        ),
        migrations.AddField(
            model_name='uom',
            name='created_at',
            field=models.DateTimeField(blank=True, default=django.utils.timezone.now, null=True, verbose_name='Created at'),
        ),
        migrations.AddField(
            model_name='uom',
            name='deleted_at',
            field=models.DateTimeField(blank=True, default=None, null=True, verbose_name='Deleted at'),
        ),
        migrations.AddField(
            model_name='uom',
            name='modified_at',
            field=models.DateTimeField(blank=True, default=None, null=True, verbose_name='Modified at'),
        ),
        migrations.AlterField(
            model_name='product',
            name='code',
            field=models.CharField(max_length=20, unique=True, verbose_name='Product CODE'),
        ),
    ]
