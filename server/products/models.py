from django.db import models
from mptt.models import MPTTModel, TreeForeignKey

# from commons.models import (Timestamp, SoftDelete)
from commons.models.timestamp import Timestamp
from commons.models.soft_delete import SoftDelete


class Product(Timestamp, SoftDelete):
    code = models.CharField(verbose_name='Product Code',
                            max_length=20,
                            unique=True)
    name = models.CharField(verbose_name='Product Name',
                            max_length=200,
                            unique=True)
    # long_name = models.TextField(verbose_name='Product Long Name',
    #                              null=True,
    #                              blank=True)
    description = models.TextField(verbose_name='Description',
                                   null=True,
                                   blank=True)
    active = models.BooleanField(verbose_name='Avaiable',
                                 default=False)
    categories = models.ManyToManyField('Category',
                                        through='ProductCategoryMap')
    brands = models.ManyToManyField('Brand',
                                    through='ProductBrandMap')
    uoms = models.ManyToManyField('Uom',
                                  through='ProductUomMap')

    class Meta:
        db_table = 'product'
        verbose_name = 'Product'
        verbose_name_plural = 'Products'
        ordering = ['code', 'name']

    def __str__(self):
        return self.name


class Category(MPTTModel, Timestamp, SoftDelete):
    name = models.CharField(verbose_name='Product category name',
                            max_length=200,
                            unique=True)
    # parent = models.ForeignKey('self',
    #                            verbose_name='Parent category',
    #                            on_delete=models.DO_NOTHING,
    #                            null=True,
    #                            blank=True,
    #                            default=None)
    parent = TreeForeignKey('self',
                            on_delete=models.PROTECT,
                            null=True,
                            blank=True,
                            related_name='children')
    products = models.ManyToManyField('Product',
                                      through='ProductCategoryMap')

    class Meta:
        db_table = 'product_category'
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    class MPTTMeta:
        order_insertion_by = ['name']

    def __str__(self):
        return self.name

    def get_parent_name(self):
        return self.name


class Brand(Timestamp, SoftDelete):
    """
    Product Brand
    """
    name = models.CharField(verbose_name='Brand name',
                            max_length=200,
                            unique=True)
    products = models.ManyToManyField('Product',
                                      through='ProductBrandMap')

    class Meta:
        db_table = 'product_brand'
        verbose_name = 'Brand'
        verbose_name_plural = 'Brands'

    def __str__(self):
        return self.name


class Uom(Timestamp, SoftDelete):
    name = models.CharField(verbose_name='Unit name',
                            max_length=100,
                            unique=True)
    symbol = models.CharField(verbose_name='Unit symbol',
                              max_length=20,
                              unique=True)
    products = models.ManyToManyField('Product',
                                      through='ProductUomMap')

    class Meta:
        db_table = 'product_uom'
        verbose_name = 'Uom'
        verbose_name_plural = 'Uoms'

    def __str__(self):
        return self.name


class ProductCategoryMap(Timestamp, SoftDelete):
    product = models.ForeignKey('Product',
                                on_delete=models.PROTECT)
    category = models.ForeignKey('Category',
                                 on_delete=models.PROTECT)

    class Meta:
        db_table = 'products_product_product_category'
        verbose_name = 'Map Product:Product Category'
        verbose_name_plural = 'Map Products:Product Categories'

    def __str__(self):
        return str(f'''Category of product {self.product.name}
                        is {self.category.name}''')


class ProductBrandMap(Timestamp, SoftDelete):
    product = models.ForeignKey('Product',
                                on_delete=models.PROTECT)
    brand = models.ForeignKey('Brand',
                              on_delete=models.PROTECT)

    class Meta:
        db_table = 'products_product_product_brand_map'
        verbose_name = 'Map Product:Product Brand'
        verbose_name_plural = 'Map Products:ProductBrands'

    def __str__(self):
        return str(f'''Brand of product {self.product.name} is
                    {self.brand.name}''')


class ProductUomMap(Timestamp, SoftDelete):
    product = models.ForeignKey('Product',
                                on_delete=models.PROTECT)
    uom = models.ForeignKey('Uom',
                            on_delete=models.PROTECT)

    class Meta:
        db_table = 'products_product_product_uom_map'
        verbose_name = 'Map Product:Product Uom'
        verbose_name_plural = 'Map Product:Product Uoms'

    def __str__(self):
        return str(f'''Uom of product {self.product.name}
                        is {self.uom.name}''')
