from rest_framework.generics import (
    ListAPIView,
    CreateAPIView,
    RetrieveAPIView,
)
from rest_framework.mixins import (
    UpdateModelMixin,
    DestroyModelMixin,
)
from rest_framework.viewsets import (
    ModelViewSet
)

from products.models import (
    Product,
    Category,
    Brand,
    Uom,
    # MapProductCategory,
    # MapProductBrand,
    # MapProductUom,
)
from .serializers import (
    ProductCreateSerializer,
    ProductSerializer,
    CategorySerializer,
    CategoryTreeSerializer,
    BrandSerializer,
    UomSerializer,
)


class ProductAPIView(CreateAPIView, ListAPIView):
    queryset = Product.objects.all()
    serializer_class = None

    def get_serializer_class(self, *args, **kwargs):
        if self.request.method == 'GET':
            return ProductSerializer
        if self.request.method == 'POST':
            return ProductCreateSerializer
        return self.serializer_class


class ProductDetailAPIView(UpdateModelMixin,
                           DestroyModelMixin,
                           RetrieveAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    lookup_field = 'id'


class CategoryViewSet(ModelViewSet):
    """
    https://www.django-rest-framework.org/api-guide/viewsets/#marking-extra-actions-for-routing
    """
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    lookup_field = 'id'


class CategoryTreeListAPIView(ListAPIView):
    queryset = Category.objects.root_nodes()
    serializer_class = CategoryTreeSerializer


class CategoryTreeDetailAPIView(RetrieveAPIView):
    serializer_class = CategoryTreeSerializer
    lookup_field = 'id'

    def get_queryset(self):
        id = self.kwargs.get('id', None)
        qs = Category.objects._mptt_filter(id=id)
        return qs


class BrandViewSet(ModelViewSet):
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer


class UomViewSet(ModelViewSet):
    queryset = Uom.objects.all()
    serializer_class = UomSerializer
