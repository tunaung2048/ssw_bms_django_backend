from django.urls import path, re_path

from products.apps import ProductsConfig
from products.api.views import (
    ProductAPIView,
    ProductDetailAPIView,
    CategoryViewSet,
    CategoryTreeListAPIView,
    CategoryTreeDetailAPIView,
    BrandViewSet,
    UomViewSet,
)
# UserListAPIView)


app_name = ProductsConfig.name

urlpatterns = [
    path(
        'products/',
        ProductAPIView.as_view(),
        name='product'
    ),
    path(
        'products/<id>',
        ProductDetailAPIView.as_view(),
        name='product-detailview',
    ),

    path(
        'categories/',
        CategoryViewSet.as_view({
            'get': 'list',
            'post': 'create'
        }),
        name='category'
    ),
    path(
        'categories/<id>',
        CategoryViewSet.as_view({
            'get': 'retrieve',
            'put': 'update',
            'patch': 'partial_update',
            'delete': 'destroy'
        }),
        name='category-detailview'
    ),
    re_path(
        r'categories-tree/$',
        CategoryTreeListAPIView.as_view(),
        name='category-tree'
    ),
    re_path(
        r'categories-tree/(?P<id>\d+)/$',
        CategoryTreeDetailAPIView.as_view(),
        name='category-tree-detailview'
    ),

    path(
        'brands/',
        BrandViewSet.as_view({
            'get': 'list',
            'post': 'create',
        }),
        name='brand',
    ),
    path(
        'brands/<id>',
        BrandViewSet.as_view({
            'get': 'retrieve',
            'put': 'update',
            'patch': 'partial_update',
            'delete': 'destroy',
        }),
        name='brand-detailview',
    ),

    path(
        'product-uoms/',
        UomViewSet.as_view({
            'get': 'list',
            'post': 'create',
        }),
        name='uom',
    ),
    path(
        'product-uoms/<id>',
        UomViewSet.as_view({
            'get': 'retrieve',
            'put': 'update',
            'patch': 'partial_update',
            'delete': 'destroy',
        }),
        name='uom-detailview'
    ),

]
