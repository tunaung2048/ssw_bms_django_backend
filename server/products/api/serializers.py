from django.core.exceptions import ObjectDoesNotExist
from rest_framework import serializers
from rest_framework.reverse import reverse as drf_reverse


from products.models import (
    Product,
    Category,
    Brand,
    Uom,
    # MapProductCategory,
    # MapProductBrand,
    # MapProductUom,
)


class ProductCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = [
            'code',
            'name',
            'description',
            'active'
        ]


class SubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = [
            'id',
            'name',
            'children',
        ]


class RecursiveField(serializers.BaseSerializer):
    """
    https://medium.com/@gilsondev/usando-django-mptt-em-apis-rest-36aad7eff3f0
    Cria instancia do serializer parente e retorna os dados
    serializados.
    """

    def to_representation(self, value):
        ParentSerializer = self.parent.parent.__class__
        serializer = ParentSerializer(value, context=self.context)
        return serializer.data

    def to_internal_value(self, data):
        ParentSerializer = self.parent.parent.__class__
        Model = ParentSerializer.Meta.model
        try:
            instance = Model.objects.get(pk=data)
        except ObjectDoesNotExist:
            raise serializers.ValidationError(
                "Objeto {0} não encontrado".format(
                    Model().__class__.__name__
                )
            )
        return instance


class MyRecursiveField(serializers.BaseSerializer):
    """ https://www.django-rest-framework.org/api-guide/serializers/#baseserializer
    This class implements the same basic API as the Serializer class:
        .data - Returns the outgoing primitive representation.
        .is_valid() - Deserializes and validates incoming data.
        .validated_data - Returns the validated incoming data.
        .errors - Returns any errors during validation.
        .save() - Persists the validated data into an object instance.

    There are four methods that can be overridden,
        depending on what functionality
        you want the serializer class to support:
        .to_representation() - Override this to support serialization,
                                for read operations.
        .to_internal_value() - Override this to support deserialization,
                                for write operations.
        .create() and .update() - Override either or both of these
                                    to support saving instances.
    """

    def to_representation(self, instance):
        ParentSerializer = self.parent.parent.__class__
        serializer = ParentSerializer(instance,
                                      context=self.context)
        return serializer.data


class CategorySerializer(serializers.ModelSerializer):
    uri = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Category
        fields = [
            'id',
            'name',
            'parent',
            'uri',
        ]
        read_only_fields = ['id']

    def get_uri(self, obj):
        request = self.context.get('request')
        return drf_reverse(
            'products-api:category-detailview',
            kwargs={'id': obj.id},
            request=request
        )


class CategoryTreeSerializer(serializers.ModelSerializer):
    uri = serializers.SerializerMethodField(read_only=True)
    subcategories = MyRecursiveField(source='children',
                                     read_only=True,
                                     many=True,
                                     required=False)

    class Meta:
        model = Category
        fields = [
            'id',
            'name',
            'parent',
            'uri',
            'subcategories'
        ]
        read_only_fields = ['id']

    def get_uri(self, obj):
        request = self.context.get('request')
        return drf_reverse(
            'products-api:category-tree-detailview',
            kwargs={'id': obj.id},
            request=request
        )


class ProductPublicSerializer(serializers.ModelSerializer):
    uri = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Product
        fields = [
            'uri',
            'id',
            'code',
            'name',
        ]
        read_only_fields = ['uri', 'id', ]

    def get_uri(self, obj):
        request = self.context.get('request')
        return drf_reverse('products-api:product-detailview',
                           kwargs={'id': obj.id},
                           request=request)


class ProductSerializer(serializers.ModelSerializer):
    uri = serializers.SerializerMethodField(read_only=True)
    # categories = CategorySerializer(read_only=True, )

    class Meta:
        model = Product
        fields = [
            'id',
            'code',
            'name',
            'description',
            'active',
            'uri',
            # 'categories',
        ]
        read_only_fields = ['id', 'uri']

    def get_uri(self, obj):
        request = self.context.get('request')
        return drf_reverse('products-api:product-detailview',
                           kwargs={'id': obj.id},
                           request=request)


class BrandSerializer(serializers.ModelSerializer):
    uri = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Brand
        fields = [
            'id',
            'name'
        ]
        read_only_fields = ['id']

    def get_uri(self, obj):
        request = self.context.get('request')
        return drf_reverse(
            'products:category-detailview',
            kwargs={'id': obj.id},
            request=request
        )


class UomSerializer(serializers.ModelSerializer):
    uri = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Uom
        fields = [
            'id',
            'name',
            'uri',
        ]
        read_only_fields = ['id']

    def get_uri(self, obj):
        request = self.context.get('request')
        print('uom')
        print(request)
        return drf_reverse(
            'products:uom-detailview',
            kwargs={'id': obj.id},
            request=request
        )
