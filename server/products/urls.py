from django.urls import path

from . import views
# from .views import CategoryIndex
from .apps import ProductsConfig

app_name = ProductsConfig.name

urlpatterns = [
    # path(
    #     'category/index',
    #     CategoryIndex.as_view(),
    #     name='category-page',
    # ),

    path(
        'category/list',
        views.category_index,
        name='category-list',
    ),

    path(
        'genres/',
        views.show_genres,
        name='genre-list',
    ),
]
