from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin

from .models import (Product,
                     Category,
                     Brand,
                     Uom,
                     ProductCategoryMap,
                     ProductBrandMap,
                     ProductUomMap,
                     )


class ProductAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'code',
        'name',
        'description',
        'active'
    ]
    fieldsets = (
        (None, {
            "fields": (
                'code',
                'name',
                'description',
                'active',
            ),
        }),
    )


class CategoryAdmin(DraggableMPTTAdmin):
    mptt_indent_field = 'name'
    list_display = ['tree_actions', 'indented_title']
    # list_display = [
    #     'tree_actions',
    #     'indented_title',
    #     'id',
    #     'name',
    #     'parent'
    # ]
    list_display_links = ['indented_title']
    fieldsets = (
        (None, {
            "fields": (
                'name',
                'parent',
            ),
        }),
    )


class BrandAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'name'
    ]
    fieldsets = (
        (None, {
            "fields": (
                'name',
            ),
        }),
    )


class UomAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'name'
    ]
    fieldsets = (
        (None, {
            "fields": (
                'name',
            ),
        }),
    )


class ProductCategoryAdminMap(admin.ModelAdmin):
    list_display = [
        'id',
        'product',
        'category'
    ]
    fieldsets = (
        (None, {
            "fields": (
                'product',
                'category',
            ),
        }),
    )


class ProductBrandAdminMap(admin.ModelAdmin):
    list_display = [
        'id',
        'product',
        'brand'
    ]
    fieldsets = (
        (None, {
            "fields": (
                'product',
                'brand',
            ),
        }),
    )


class ProductUomAdminMap(admin.ModelAdmin):
    list_display = [
        'id',
        'product',
        'uom'
    ]
    fieldsets = (
        (None, {
            "fields": (
                'product',
                'uom',
            ),
        }),
    )


admin.site.register(Product, ProductAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Brand, BrandAdmin)
admin.site.register(Uom, UomAdmin)
admin.site.register(ProductCategoryMap, ProductCategoryAdminMap)
admin.site.register(ProductBrandMap, ProductBrandAdminMap)
admin.site.register(ProductUomMap, ProductUomAdminMap)
