import json
from django.core.exceptions import ImproperlyConfigured


""" Load environment variables from external json file """
with open('env.sample.json') as f:
    env_file = json.loads(f.read())


def get_env_variable(key_name, env_file=env_file):
    try:
        return env_file[key_name]
    except KeyError:
        error_msg = f'Set the {key_name} environment variable from external source.'  # noqa
        raise ImproperlyConfigured(error_msg)
# ::usage::
# API_KEY=get_env_variable('MY_API_KEY')


""" Get environment variables """
# def get_env_variable(var_name):
#     """ Get Environment Variable
#         to get value from environment variable
#     """
#     try:
#         return os.environ[var_name]
#     except KeyError:
#         error_msg = f'Set the {var_name} environment variable.'
#         raise ImproperlyConfigured(error_msg)
