from os import path
from environ import Env

from ssw_bms.settings.django_settings.base import BASE_DIR

env = Env(
    # set casting, default value
    HOSTNAME=(str, '127.0.0.1'),
    PORT=(str, 8000),
    SUBDOMAIN=(str, ''),
    DEBUG=(bool, False),
    SECRET_KEY=(str, 'so-called-super-secure-dummy-secret-key'),
    CORS_ALLOW_ALL_ORIGINS=(bool, False),
    DATABASE_HOST=(str, '127.0.0.1'),
    DATABASE_PORT=(str, 5432),
    DATABASE_NAME=(str, 'dev_db'),
    DATABASE_USERNAME=(str, 'dev_db_user'),
    DATABASE_PASSWORD=(str, ''),
    STATIC_ROOT=(str, '~/media/staticfiles/'),
    MEDIA_ROOT=(str, '~/media/mediafiles/'),
)

# read .env file located at prj_root/.env
PRJ_ROOT = path.abspath(path.dirname(path.abspath(BASE_DIR)))
file_path = path.join(PRJ_ROOT, '.env')
env.read_env(env_file=file_path)
# environ.Env.read_env(env('ENV_FILE', env_file))
