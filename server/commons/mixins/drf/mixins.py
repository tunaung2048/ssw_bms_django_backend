from django.shortcuts import get_object_or_404

# https://www.django-rest-framework.org/api-guide/generic-views/#creating-custom-mixins


class MultipleFieldLookupMixin:
    """
    Apply this mixin to any view or viewset to get multiple field filtering
    based on a `lookup_fields` attribute, instead of the default single field filtering.
    """

    def get_object(self):
        queryset = self.get_queryset()             # Get the base queryset
        queryset = self.filter_queryset(queryset)  # Apply any filter backends
        filter = {}
        if self.lookup_fields:
            for field in self.lookup_fields:
                param = self.kwargs.get(field)
                print(f'param: {param}')
                if param is not None:
                    # add to filter
                    filter[field] = param
                    obj = get_object_or_404(
                        queryset, **filter)  # Lookup the object
                    self.check_object_permissions(self.request, obj)
                    print(f'>> {obj}')
                    return obj
