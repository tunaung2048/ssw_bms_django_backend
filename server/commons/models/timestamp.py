from django.db import models
from django.utils import timezone


class Timestamp(models.Model):
    """ Abstract model for timestamp fields
        created_at, modified_at
    """
    created_at = models.DateTimeField(verbose_name='Created at',
                                      null=True,
                                      blank=True,
                                      default=timezone.now)
    modified_at = models.DateTimeField(verbose_name='Modified at',
                                       null=True,
                                       blank=True,
                                       default=None)

    class Meta:
        abstract = True
