from django.db import models


class SoftDelete(models.Model):
    """ Abstract Model for Soft delte datetime field
        deleted_at
    """
    deleted_at = models.DateTimeField(verbose_name='Deleted at',
                                      null=True,
                                      blank=True,
                                      default=None)

    class Meta:
        abstract = True
