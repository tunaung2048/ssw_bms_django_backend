from uuid import uuid4
from django.db import models


class PkUUID(models.Model):
    """ Abstract model for Primary key UUID
        id
    """
    id = models.UUIDField(verbose_name='ID',
                          primary_key=True,
                          default=uuid4)

    class Meta:
        abstract = True
