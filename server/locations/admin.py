from django.contrib import admin

from .models import (
    InventoryLocationType,
    InventoryLocation,
    InventoryLocationDetail
)


class InventoryLocationTypeAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'name',
    ]


class InventoryLocationAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'name',
    ]


class InventoryLocationDetailAdmin(admin.ModelAdmin):
    list_display = [
        'id',
    ]


admin.site.register(InventoryLocationType, InventoryLocationTypeAdmin)
admin.site.register(InventoryLocation, InventoryLocationAdmin)
admin.site.register(InventoryLocationDetail, InventoryLocationDetailAdmin)
