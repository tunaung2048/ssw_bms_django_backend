from django.db import models

from commons.models import (timestamp)


class InventoryLocationType(timestamp.Timestamp):
    name = models.CharField(verbose_name='Inventory location type',
                            max_length=100,
                            unique=True)
    description = models.TextField(verbose_name='description',
                                   max_length=255)

    class Meta:
        db_table = 'locations_inventory_location_type'
        verbose_name = 'Inventory location type'
        verbose_name_plural = 'Inventory location types'

    def __str__(self):
        return self.name


class InventoryLocation(timestamp.Timestamp):
    name = models.CharField(verbose_name='Inventory location name',
                            max_length=100,
                            unique=True)
    active = models.BooleanField(verbose_name='Active',
                                 default=False)

    class Meta:
        db_table = 'locations_inventory_location'
        verbose_name = 'Inventory location'
        verbose_name_plural = 'Inventory locations'

    def __str__(self):
        return self.name


class InventoryLocationDetail(timestamp.Timestamp):
    inventory = models.ForeignKey('InventoryLocation',
                                  on_delete=models.PROTECT,
                                  related_name='inventory_locations')

    class Meta:
        db_table = 'locations_inventory_location_detail'
        verbose_name = 'Inventory location detail'
        verbose_name_plural = 'Inventory location details'

    def __str__(self):
        return self.inventory.name
