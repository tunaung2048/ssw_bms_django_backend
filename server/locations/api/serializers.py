

from rest_framework.serializers import ModelSerializer

from locations.models import (
    InventoryLocationType,
    InventoryLocation,
    InventoryLocationDetail,
)


# class InventoryLocationTypeCreateSerializer(ModelSerializer):
#     class Meta:
#         model = InventoryLocationType
#         fields = [
#             'name',
#             'description',
#         ]

class InventoryLocationTypeSerializer(ModelSerializer):
    class Meta:
        model = InventoryLocationType
        fields = [
            'id',
            'name',
            'description'
        ]
        read_only_fields = ['id']

# class InventoryLocationCreateSerializer(ModelSerializer):
#     class Meta:
#         model = InventoryLocation
#         fields = [
#             'name',
#             'active',
#         ]


class InventoryLocationSerializer(ModelSerializer):
    class Meta:
        model = InventoryLocation
        fields = [
            'id',
            'name',
            'active',
        ]
        read_only_fields = ['id']


class InventoryLocationDetailSerializer(ModelSerializer):
    class Meta:
        model = InventoryLocationDetail
        fields = [
            'id',
            'inventory',
        ]
        read_only_fields = ['id']
