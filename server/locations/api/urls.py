from django.urls import path

from locations.apps import LocationsConfig
from locations.api.views import (
    InventoryLocationTypeViewSet,
    InventoryLocationViewSet,
    InventoryLocationDetailViewSet,
)


app_name = LocationsConfig.name

urlpatterns = [
    path(
        'inventory/location-types/',
        InventoryLocationTypeViewSet.as_view({
            'get': 'list',
            'post': 'create',
        }),
        name='inventory-transaction-type'
    ),
    path(
        'inventory/location-types/<id>',
        InventoryLocationTypeViewSet.as_view({
            'get': 'retrieve',
            'put': 'update',
            'patch': 'partial_update',
            'delete': 'destroy'
        }),
        name='inventory-location-type-detailview'
    ),

    path(
        'inventory/locations/',
        InventoryLocationViewSet.as_view({
            'get': 'list',
            'post': 'create',
        }),
        name='inventory-location',
    ),
    path(
        'inventory/locations/<id>',
        InventoryLocationViewSet.as_view({
            'get': 'retrieve',
            'put': 'update',
            'patch': 'partial_update',
            'delete': 'destroy',
        }),
        name='inventory-location-detailview',
    ),
    path(
        'inventory/locations-details',
        InventoryLocationViewSet.as_view({
            'get': 'list',
            'post': 'create',
        })
    ),
    path(
        'inventory/locations-details/<id>',
        InventoryLocationDetailViewSet.as_view({
            'get': 'retrieve',
            'put': 'update',
            'patch': 'partial_update',
            'delete': 'destroy',
        })
    ),
]
