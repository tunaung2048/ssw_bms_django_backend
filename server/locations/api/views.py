# from rest_framework.generics import (
#     ListAPIView,
#     CreateAPIView,
#     RetrieveAPIView,
# )
from rest_framework.viewsets import (
    ModelViewSet
)

from locations.models import (
    InventoryLocationType,
    InventoryLocation,
    InventoryLocationDetail,
)
from locations.api.serializers import (
    InventoryLocationTypeSerializer,
    InventoryLocationSerializer,
    InventoryLocationDetailSerializer,
)


# class InventoryLocationTypeAPIView(CreateAPIView, ListAPIView):
#     queryset = InventoryLocationType.objects.all()
#     serializer_class = InventoryLocationTypeSerializer

# class InventoryLocatinTypeDetailAPIView():


# class InventoryLocationAPIView(CreateAPIView, ListAPIView):


class InventoryLocationTypeViewSet(ModelViewSet):
    queryset = InventoryLocationType.objects.all()
    serializer_class = InventoryLocationTypeSerializer
    lookup_field = 'id'


class InventoryLocationViewSet(ModelViewSet):
    queryset = InventoryLocation.objects.all()
    serializer_class = InventoryLocationSerializer
    lookup_field = 'id'


class InventoryLocationDetailViewSet(ModelViewSet):
    queryset = InventoryLocationDetail.objects.all()
    serializer_class = InventoryLocationDetailSerializer
    lookup_field = 'id'
