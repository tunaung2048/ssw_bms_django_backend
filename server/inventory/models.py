from django.conf import settings
from django.db import models

from inventory.apps import InventoryConfig
from commons.models import (timestamp,)
from products.models import (Product, Uom)
from locations.models import (InventoryLocationType,
                              InventoryLocation,
                              InventoryLocationDetail)


User = settings.AUTH_USER_MODEL
# class Department():
#     """
#         warehouse 1
#         store 1
#         hr depratment 1
#         finance department 1
#         service center 1
#     """
# class DepartmentType():
#         warehouse
#         store
#         hr
#         finance
#         service


class InventoryTransactionType(timestamp.Timestamp):
    name = models.CharField(verbose_name='Inventory transaction type name',
                            max_length=100,
                            unique=True,)
    description = models.TextField(verbose_name='description',
                                   max_length=255,
                                   null=True,
                                   blank=True)

    class Meta:
        db_table = 'inventory_inventory_transaction_type'
        verbose_name = 'Inventory transaction type'
        verbose_name_plural = 'Inventory transaction types'

    def __str__(self):
        return self.name
    # loss.[inventory_transactions].objects.all()
    # [inventory_transaction] == related_name in child model
    # get all record of inventory_transactions that are have
    # type of 'loss' associated


class InventoryTransaction(timestamp.Timestamp):
    issued_date = models.DateTimeField(verbose_name='Issued Date')
    inventory_code = models.CharField(verbose_name='Inventory code',
                                      max_length=20,
                                      unique=True)
    # quantity = models.DecimalField(verbose_name='Quantity',
    #                                max_digits=8,
    #                                decimal_places=2,)
    description = models.TextField(verbose_name='description',
                                   max_length=255,
                                   null=True,
                                   blank=True)
    inventory_transaction_type = models.ForeignKey(
        InventoryTransactionType,
        on_delete=models.PROTECT,
        related_name='inventory_transaction_types')
    user = models.ForeignKey(User,
                             on_delete=models.PROTECT,
                             related_name='created_by')

    class Meta:
        db_table = 'inventory_inventory_transaction'
        verbose_name = 'Inventory transaction'
        verbose_name_plural = 'Inventory transactions'

    def __str__(self):
        return self.inventory_code


class InventoryTransactionDetail(timestamp.Timestamp):
    quantity = models.DecimalField(verbose_name='Quantity',
                                   max_digits=8,
                                   decimal_places=2,)
    inventory_transaction = models.ForeignKey(
        InventoryTransaction,
        on_delete=models.PROTECT,
        related_name='inventory_transactions')
    product = models.ForeignKey(
        Product,
        on_delete=models.PROTECT,
        # related_name='product_in_inventory')
    )
    # product instance can use:
    # product1.inventory_transaction_detail_set.objects.all()
    # product_variation = models.ForeignKey(ProductVariation)
    uom = models.ForeignKey(
        Uom,
        on_delete=models.PROTECT)
    inventory_location = models.ForeignKey(
        InventoryLocation,
        on_delete=models.PROTECT)
    # get inventory_location of inventory transaction details

    class Meta:
        db_table = 'inventory_inventory_transaction_detail'
        verbose_name = 'inventory_transaction_detail'
        verbose_name_plural = 'inventory_transaction_details'

    def __str__(self):
        return self.inventory_transaction.inventory_code

# class InventoryManagementMethods(models.Model):
#     """Inventory Management Methods for FIFO, LIFO,..
#         ** you could use django's choice list instead.
#         ** https://docs.djangoproject.com/en/2.2/ref/models/fields/#choices
#     """
#     name= models.CharField(verbose_name='Method name',
#                             max_length=100)
#     name_short = models.CharField(verbose_name='Method name name (short name)',
#                                     max_length=10)
#     description = models.TextField(verbose_name='description',
#                                     null=True
#     )
#     active = models.BooleanField(verbose_name='Active',
#                                     default=True)

    # class Meta:
    #     db_table= 'inventory_inventory_management_method'
    #     verbose_name='Inventory Management Method'
    #     verbose_name_plural = 'Inventory Management Methods'

# class InventoryTransaction():
