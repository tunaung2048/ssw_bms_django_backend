from django.contrib import admin

from .models import (
    InventoryTransactionType,
    InventoryTransaction,
    InventoryTransactionDetail,
)


class InventoryTransactionTypeAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'name',
    ]


class InventoryTransactionAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'inventory_code',
        'inventory_transaction_type',
        'user',

    ]


class InventoryTransactionDetailAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'inventory_transaction',
        'product',
        'quantity',
        'uom',
    ]


admin.site.register(InventoryTransactionType, InventoryTransactionTypeAdmin)
admin.site.register(InventoryTransaction, InventoryTransactionAdmin)
admin.site.register(InventoryTransactionDetail,
                    InventoryTransactionDetailAdmin)
