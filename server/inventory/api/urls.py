from django.urls import path

from inventory.apps import InventoryConfig
from inventory.api.views import (
    InventoryTransactionTypeViewSet,
    InventoryTransactionViewSet,
    InventoryTransactionDetailViewSet,
    InventoryAPIView,
)

app_name = InventoryConfig.name

urlpatterns = [
    path(
        'inventory/',
        InventoryAPIView.as_view(),
        name='inventory',
    ),
    path(
        'inventory-transaction-types/',
        InventoryTransactionTypeViewSet.as_view({
            'get': 'list',
            'post': 'create',
        }),
        name='inventory-transaction-type',
    ),
    path(
        'inventory-transaction-types/<id>',
        InventoryTransactionTypeViewSet.as_view({
            'get': 'retrieve',
            'put': 'update',
            'patch': 'partial_update',
            'delete': 'destroy',
        }),
        name='inventory-transaction-type-detail-detailview',
    ),
    path(
        'inventory-transactions/',
        InventoryTransactionViewSet.as_view({
            'get': 'list',
            'post': 'create',
        }),
        name='inventory-transaction',
    ),
    path(
        'inventory-transactions/<id>',
        InventoryTransactionViewSet.as_view({
            'get': 'retrieve',
            'put': 'update',
            'patch': 'partial_update',
            'delete': 'destroy',
        }),
        name='inventory-transaction-detailview',
    ),
    path(
        'inventory-transaction-details/',
        InventoryTransactionDetailViewSet.as_view({
            'get': 'list',
            'post': 'create',
        }),
        name='inventory-transaction-detail',
    ),
    path(
        'inventory-transaction-details/<id>',
        InventoryTransactionDetailViewSet.as_view({
            'get': 'retrieve',
            'put': 'update',
            'patch': 'partial_update',
            'delete': 'destroy',
        }),
        name='inventory-transaction-detail-detailview',
    ),
]
