from rest_framework.views import (APIView)
from rest_framework.response import Response
from rest_framework.viewsets import (
    ModelViewSet,
)

from inventory.models import (
    InventoryTransactionType,
    InventoryTransaction,
    InventoryTransactionDetail,
)
from inventory.api.serializers import (
    InventoryTransactionTypeSerializer,
    InventoryTransactionTypePublicSerializer,
    InventoryTransactionSerializer,
    InventoryTransactionCreateSerializer,
    InventoryTransactionDetailSerializer,
    InventorySerializer,
)


class InventoryTransactionTypeViewSet(ModelViewSet):
    queryset = InventoryTransactionType.objects.all()
    serializer_class = InventoryTransactionTypeSerializer
    lookup_field = 'id'

    def get_serializer_class(self, *args, **kwargs):
        if self.action == 'list':
            return InventoryTransactionTypePublicSerializer
        return InventoryTransactionTypeSerializer


class InventoryTransactionViewSet(ModelViewSet):
    queryset = InventoryTransaction.objects.all()
    serializer_class = InventoryTransactionSerializer
    lookup_field = 'id'

    def get_serializer_class(self, *args, **kwargs):
        if self.request.method == 'POST':
            return InventoryTransactionCreateSerializer
        return InventoryTransactionSerializer


class InventoryTransactionDetailViewSet(ModelViewSet):
    queryset = InventoryTransactionDetail.objects.all()
    serializer_class = InventoryTransactionDetailSerializer
    lookup_field = 'id'


class InventoryAPIView(APIView):
    # inventory_master = InventoryTransaction.objects.all()
    # inventory_detail = InventoryTransactionDetail.objects.all()
    qs = InventoryTransactionDetail.objects.all()

    def get(self, request, format=None):
        list = {
            'issued_date': '1-Jan-2020 13:30:04',
            'inventory_code': 'INV-0000-0031',
            'product': 'Panel Meter 44L1 300VAC',
            'quantity': 500,
            'uom': 'pcs'
        }
        return Response(list)
