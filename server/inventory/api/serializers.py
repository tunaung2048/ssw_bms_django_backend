from rest_framework.serializers import (
    Serializer,
    ModelSerializer,
    SerializerMethodField,
)
from rest_framework.reverse import reverse as drf_reverse

from products.api.serializers import (
    UomSerializer,
    ProductPublicSerializer,
)
from locations.api.serializers import (InventoryLocationSerializer)
from accounts.api.serializers import UserPublicSerializer
from inventory.models import (
    InventoryTransactionType,
    InventoryTransaction,
    InventoryTransactionDetail,
)


class InventoryTransactionTypePublicSerializer(ModelSerializer):
    uri = SerializerMethodField(read_only=True)

    class Meta:
        model = InventoryTransactionType
        fields = [
            'id',
            'name',
            'uri',
        ]
        read_only_fields = ['id', ]

    def get_uri(self, obj):
        request = self.context.get('request')
        return drf_reverse(
            'inventory-api:inventory-transaction-type-detail-detailview',
            kwargs={'id': obj.id},
            request=request
        )


class InventoryTransactionDetailPublicSerializer(ModelSerializer):
    product = ProductPublicSerializer(read_only=True)
    uom = UomSerializer(read_only=True)

    class Meta:
        model = InventoryTransactionDetail
        fields = [
            'id',
            'product',
            'quantity',
            'uom',
            'inventory_transaction',
            'inventory_location',
        ]
        read_only_fields = ['id', ]


class InventoryTransactionTypeSerializer(ModelSerializer):
    class Meta:
        model = InventoryTransactionType
        fields = [
            'id',
            'name',
        ]
        read_only_fields = ['id', ]


class InventoryTransactionCreateSerializer(ModelSerializer):
    class Meta:
        model = InventoryTransaction
        fields = [
            'id',
            'issued_date',
            'inventory_code',
            'description',
            'inventory_transaction_type',
            'user'
        ]
        read_only_fields = ['id', ]


class InventoryTransactionSerializer(ModelSerializer):
    inventory_transaction_type = InventoryTransactionTypePublicSerializer(
        read_only=True)
    details = SerializerMethodField(read_only=True)
    # details = SerializerMethodField(read_only=True)
    user = UserPublicSerializer(read_only=True)
    uri = SerializerMethodField(read_only=True)

    class Meta:
        model = InventoryTransaction
        fields = [
            'id',
            'issued_date',
            'inventory_code',
            'description',
            'inventory_transaction_type',
            'user',
            'uri',
            'details'
        ]
        read_only_fields = ['id', ]

    def get_uri(self, obj):
        request = self.context.get('request')
        return drf_reverse(
            'inventory-api:inventory-transaction-detailview',
            kwargs={'id': obj.id},
            request=request
        )

    def get_details(self, obj):
        qs = InventoryTransactionDetail.objects.filter(
            inventory_transaction=obj.id)
        serializer = InventoryTransactionDetailPublicSerializer(qs, many=True)
        data = serializer.data
        return data


class InventoryTransactionDetailSerializer(ModelSerializer):
    uri = SerializerMethodField(read_only=True)
    # inventory_location

    class Meta:
        model = InventoryTransactionDetail
        fields = [
            'id',
            'quantity',
            'inventory_transaction',
            'product',
            'uom',
            'inventory_location',
            'uri',
        ]
        read_only_fields = ['id', ]

    def get_uri(self, obj):
        request = self.context.get('request')
        return drf_reverse(
            'inventory-api:inventory-transaction-detail-detailview',
            kwargs={'id': obj.id},
            request=request
        )


class InventorySerializer(Serializer):
    """Custom serializer that combines
        InventoryTransactionSerializer and InventoryTransactionDetailSerializer
    """
    inventory_tran_serializer = InventoryTransactionSerializer
    inventory_tran_detail_serializer = InventoryTransactionDetailSerializer

    # def create(self, validated_data):
    #     print(validated_data)
    # def list(self):
    #     return
#
