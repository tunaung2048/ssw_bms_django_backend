from django.contrib.auth import get_user_model, authenticate
from rest_framework.generics import CreateAPIView, ListAPIView
from rest_framework import generics, mixins
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_framework_simplejwt.tokens import RefreshToken

from accounts.models import UserProfile
from .serializers import (UserAuthSerializer,
                          CustomTokenObtainPairSerializer,
                          UserRegisterSerializer,
                          UserSerializer,
                          ProfileCreateSerializer,
                          ProfileSerializer,
                          )


User = get_user_model()


# POST /api/auth
class UserAuthAPIView(TokenObtainPairView):
    # permission_classes =
    serializer_class = CustomTokenObtainPairSerializer


# Unused
# class UserAuthAPIViewOne(CreateAPIView):
#     # authentication_classes = None
#     permission_classes = [AllowAny]
#     serializer_class = UserAuthSerializer

#     def get_tokens_for_user(self, user=None):
#         refresh = RefreshToken.for_user(user)
#         return {
#             'refresh': str(refresh),
#             'access': str(refresh.access_token)
#         }

#     def post(self, request, *args, **kwargs):
#         if request.user.is_authenticated:
#             return Response(
#                 {
#                     'detail': 'You are already authnticated.'
#                 },
#                 status=400
#             )

#         data = request.data
#         username = data.get('username')
#         password = data.get('password')
#         user = authenticate(username=username, password=password)
#         print(f'request data:: {data}')
#         print(f'user > {user}')
#         if user is None:
#             return Response({'message': 'Invalid Credentials.'})
#         # use user object to get jwt or something

#         token = self.get_tokens_for_user(user)
#         return Response({'token': token})


# GET, POST
class UserAPIView(CreateAPIView, ListAPIView):
    # queryset = User.objects.all() # override with get_queryset()
    # permission_classes = None
    # authentication_classes = None
    # serializer_class = None

    def get_serializer_class(self, *args, **kwargs):
        if self.request.method == 'GET':
            return UserSerializer
        if self.request.method == 'POST':
            return UserRegisterSerializer
        # super().get_serializer_class()
        # return self.serializer_class
        return UserSerializer

    def get_queryset(self):
        # or use queryset = User.objects.all()
        qs = User.objects.all().filter()
        qs = qs.order_by('username')
        return qs


# GET, PUT, PATCH, DELETE
class UserDetailAPIView(mixins.UpdateModelMixin,
                        mixins.DestroyModelMixin,
                        generics.RetrieveAPIView):
    """
    GET Details
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer
    lookup_field = 'id'
    # lookup_fields = ['id', 'uuid']


# GET, POST
class ProfileAPIView(CreateAPIView, ListAPIView):
    queryset = UserProfile.objects.all()
    serializer_class = None

    def get_serializer_class(self, *args, **kwargs):
        if self.request.method == 'GET':
            return ProfileSerializer
        if self.request.method == 'POST':
            return ProfileCreateSerializer
        return ProfileSerializer


# GET, PUT, PATCH, DELETE
class ProfileDetailAPIView(mixins.UpdateModelMixin,
                           mixins.DestroyModelMixin,
                           generics.RetrieveAPIView):
    queryset = UserProfile.objects.all()
    serializer_class = ProfileSerializer
    lookup_field = 'id'
