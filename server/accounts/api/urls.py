from django.urls import path, re_path

from accounts.apps import AccountsConfig
from accounts.api.views import (UserAuthAPIView,
                                UserAPIView,
                                UserDetailAPIView,
                                # Profile
                                ProfileAPIView,
                                ProfileDetailAPIView,
                                )
# UserListAPIView)


app_name = AccountsConfig.name

urlpatterns = [
    # /api/auth/
    path('auth/',
         UserAuthAPIView.as_view(),
         name='authentication-api'),

    # /api/users
    re_path(r'users/?$',
            UserAPIView.as_view(),
            name='user'),
    # /api/users/1
    re_path(r'users/(?P<id>\d+)/?$',
            UserDetailAPIView.as_view(),
            name='user-detail'),

    # /api/profiles
    path('profiles/',
         ProfileAPIView.as_view(),
         name='profile'),
    # /api/profiles
    path('profiles/<id>/',
         ProfileDetailAPIView.as_view(),
         name='profile-detail')

]
