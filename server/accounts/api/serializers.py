from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.reverse import reverse as drf_reverse
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from accounts.models import UserProfile


User = get_user_model()


# public serializers
class UserPublicSerializer(serializers.ModelSerializer):
    uri = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = User
        fields = [
            'id',
            'uuid',
            'username',
            'uri'
        ]

    def get_uri(self, obj):
        request = self.context.get('request')
        return drf_reverse('accounts-api:user-detail',
                           kwargs={'id': obj.id},
                           request=request)


class ProfilePublicSerializer(serializers.ModelSerializer):
    uri = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = UserProfile
        fields = [
            'id',
            'fullname',
            'display_name',
            'profile_pic',
            'uri'
        ]

    def get_uri(self, obj):
        request = self.context.get('request')
        return drf_reverse('accounts-api:profile-detail',
                           kwargs={'id': obj.id},
                           request=request)


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        # Add custom claims
        token['name'] = user.username
        token['message'] = 'Use token in authorization header to access in your client.'  # noqa
        # ...

        return token


class UserAuthSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'username',
            'password'
        ]


class UserRegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True,
                                     style={'input_type': 'password'})
    confirm_password = serializers.CharField(write_only=True,
                                             style={'input_type': 'password'})

    class Meta():
        model = User
        fields = [
            'username',
            # 'uuid', #auto-generated uuid
            'email',
            'password',
            'confirm_password',
            'staff',
            'admin',
            'active'
        ]
        # extra_kwargs = {'password': {'write_only': True}}

    def validate(self, data):
        pw1 = data.get('password')
        pw2 = data.get('confirm_password')
        if pw1 != pw2:
            raise serializers.ValidationError('Password don\'t match')
        return data

    def validate_username(self, value):
        qs = User.objects.filter(username__iexact=value)
        if qs.exists():
            raise serializers.ValidationError('Username already taken.')
        return value

    def validate_email(self, value):
        qs = User.objects.filter(email__iexact=value)
        if qs.exists():
            raise serializers.ValidationError('Email already taken.')
        return value

    def create(self, validated_data):
        user_obj = User(username=validated_data.get('username'),
                        email=validated_data.get('email'),
                        staff=validated_data.get('staff'),
                        admin=validated_data.get('admin'),
                        active=validated_data.get('active')
                        )
        user_obj.set_password(validated_data.get('password'))
        user_obj.save()
        return user_obj


class UserSerializer(serializers.ModelSerializer):
    """
    Serializer for User
    GET PUT PATCH DELETE
    """
    uri = serializers.SerializerMethodField(read_only=True)
    profile = serializers.SerializerMethodField(read_only=True)
    # profile = ProfilePublicSerializer(read_only=True)
    # profile = serializers.PrimaryKeyRelatedField(
    #     queryset=UserProfile.objects.get(user=id), read_only=True)

    class Meta():
        model = User
        fields = [
            'id',
            'uuid',
            'username',
            'email',
            'staff',
            'admin',
            'active',
            'uri',
            'profile',
        ]
        # read_only_fields = ['profile']

    def get_uri(self, obj):
        request = self.context.get('request')
        return drf_reverse('accounts-api:user-detail',
                           kwargs={'id': obj.id},
                           request=request)

    def get_profile(self, obj):
        request = self.context.get('request')
        qs = UserProfile.objects.filter(user=obj)
        profile = ProfilePublicSerializer(qs.first(),
                                          context={'request': request}
                                          ).data
        return profile


class ProfileCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = [
            'user',
            'fullname',
            'display_name',
        ]


class ProfileSerializer(serializers.ModelSerializer):
    uri = serializers.SerializerMethodField(read_only=True)
    user = UserPublicSerializer(read_only=True)

    class Meta():
        model = UserProfile
        fields = [
            'id',
            'fullname',
            'display_name',
            'profile_pic',
            'user',
            'uri',
            # 'account',
        ]

    def get_uri(self, obj):
        request = self.context.get('request')
        return drf_reverse('accounts-api:profile-detail',
                           kwargs={'id': obj.id},
                           request=request)


class ProfileInlineUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = [
            # 'id',
            'fullname',
            'profile_pic'
        ]
