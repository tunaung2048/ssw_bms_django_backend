from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import ReadOnlyPasswordHashField

User = get_user_model()


class UserCreationForm(forms.ModelForm):
    """
        User creation form for <Admin Interface>
    """
    password1 = forms.CharField(label='Password',
                                widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirm password',
                                widget=forms.PasswordInput)
    email = forms.EmailField(label='Email',
                             widget=forms.EmailInput)

    class Meta():
        model = User
        fields = [
            'username',
            'email',
            'password',
            'staff',
            'admin',
            'active',
            # 'groups',
        ]

    def is_valid(self):
        print(self.errors)
        return super(UserCreationForm, self).is_valid()

    def clean_email(self):
        email = self.cleaned_data.get('email')
        print(f'your email is: {email}')
        qs = User.objects.filter(email=email)
        if qs.exists():
            raise forms.ValidationError('Email is already taken.')
        print('email is clean')
        return email

    def clean_password(self):
        """
            Check confirm password matches
        """
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Passwords not match.')
        print('password is clean')
        return password1

    def save(self, commit=True):
        """
            hash password before save
        """
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """
        A form for updating users. Includes all the fields on
        the user, but replaces the password field with admin's
        password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta():
        model = User
        fields = [
            'username',
            'email',
            'password',
            'staff',
            'admin',
            'active',
            # 'groups'
        ]

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]
