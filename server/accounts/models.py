from uuid import uuid4
from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.utils import timezone

from .managers import UserManager
from .utils import upload_profile_pic


class User(AbstractBaseUser, PermissionsMixin):
    """ Custom user model
        Custom users and django.contrib.admin
        If you want your custom user model to also work with the admin,
        your user model must define some additional attributes and methods.
        These methods allow the admin to
        control access of the user to admin content:
        is_staff
            Returns True if the user is allowed
            to have access to the admin site.
        is_active
            Returns True if the user account is currently active.
        has_perm(perm, obj=None):
            Returns True if the user has the named permission.
            If obj is provided, the permission needs to be
            checked against a specific object instance.
        has_module_perms(app_label):
            Returns True if the user has permission
            to access models in the given app.
    """
    uuid = models.UUIDField(verbose_name='User UUID',
                            default=uuid4,
                            editable=False)
    username = models.CharField(verbose_name='Username',
                                max_length=20,
                                unique=True)
    email = models.EmailField(verbose_name='Email',
                              unique=True)
    date_joined = models.DateTimeField(verbose_name='Date joined',
                                       default=timezone.now)
    staff = models.BooleanField(verbose_name='Is staff user',
                                default=False)
    admin = models.BooleanField(verbose_name='Is super user',
                                default=False)
    active = models.BooleanField(verbose_name='Is user active',
                                 default=False)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    objects = UserManager()

    class Meta:
        db_table = 'auth_user'
        # default_permissions = ()
        permissions = [
            ('can_authenticate', 'Can authenticate'),
            ('can_register', 'Can register new user'),
            ('can_change_password', 'Can change password'),
            ('can_change_username', 'Can change username'),
        ]

    @property
    def is_staff(self):
        return self.staff

    @property
    def is_superuser(self):
        return self.admin

    @property
    def is_active(self):
        return self.active

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        """ Does user have specified permssion?
            Does user have permssion for that object?
        """
        return True

    def has_module_perms(self, app_label):
        """ Does user have permission to use app `app_label`?"""
        return True

    # Override model's save method
    # ** won't work if primary key is custom
    # def save(self, *args, **kwargs):
    #     super(User, self).save(*args, **kwargs)


class UserProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,
                                on_delete=models.PROTECT)
    fullname = models.CharField(verbose_name='Full name',
                                max_length=20,
                                null=True,
                                blank=True)
    display_name = models.CharField(verbose_name='Display name',
                                    max_length=20,
                                    null=True,
                                    blank=True)
    profile_pic = models.ImageField(verbose_name='Profile picture',
                                    upload_to=upload_profile_pic)

    def __str__(self):
        return self.display_name

    class Meta():
        db_table = 'user_profile'
        verbose_name = 'User Profile'
        verbose_name_plural = 'User Profiles'
