from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from .models import UserProfile
from .forms import UserCreationForm, UserChangeForm

User = get_user_model()


class UserAdmin(BaseUserAdmin):
    add_form = UserCreationForm
    form = UserChangeForm

    list_display = [
        'id',
        'uuid',
        'username',
        'email',
        'date_joined',
        'staff',
        'admin',
        'active'
    ]
    list_display_links = ['uuid', 'username', 'email']
    list_filter = [
        # ('Role', {
        #     'fields': (
        #         'is_staff',
        #         'is_active',
        #     )
        # }),
    ]
    search_fields = ['username', 'email']
    fieldsets = (
        (None, {
            "fields": (
                'username',
                'email',
                'password',
                'staff',
                'admin',
                'active',
            ),
        }),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'username',
                'email',
                'password1',
                'password2',
                'staff',
                'admin',
                'active',
            ),
        }),
    )


class UserProfileAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'fullname',
        'display_name',
        'profile_pic'
    ]


admin.site.register(User, UserAdmin)
admin.site.register(UserProfile, UserProfileAdmin)
