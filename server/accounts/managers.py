from django.contrib.auth.models import BaseUserManager


class UserManager(BaseUserManager):
    """
    Custom User manager
    """

    def create_user(self,
                    username,
                    email,
                    password=None,
                    is_staff=False, is_superuser=False, is_active=False):
        """
        base create user
        """
        if not username:
            raise ValueError('Username is required.')
        if not email:
            raise ValueError('Email is required.')

        user = self.model(
            username=username,
            # email=self.normalize_email(email)
        )
        user.set_password(password)
        user.email = email
        user.is_staff = is_staff
        user.is_superuser = is_superuser
        user.is_active = is_active
        user.save(using=self._db)
        return user

    def create_staffuser(self,
                         username,
                         email,
                         password=None,
                         is_staff=True,
                         is_superuser=False,
                         is_active=False):
        """
        Create Staff user
        """
        return self.create_user(username,
                                email,
                                password,
                                is_staff=True,
                                is_superuser=superuser,
                                is_active=active)

    def create_superuser(self,
                         username,
                         email,
                         password=None,
                         is_staff=True,
                         is_superuser=True,
                         is_active=True):
        """
        Create Superuser - python manage.py createsuperuser
        """
        return self.create_user(username,
                                email,
                                password,
                                is_staff,
                                is_superuser,
                                is_active)
