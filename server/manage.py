#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys

from commons.utils.env_utils import env

DJANGO_ENV = env('DJANGO_ENV')


def get_django_settings_module():
    if DJANGO_ENV == 'DEVELOPMENT':
        return 'ssw_bms.settings.dev'
    if DJANGO_ENV == 'PRODUCTION':
        return 'ssw_bms.settings.prod'
    if DJANGO_ENV == 'TESTING':
        return 'ssw_bms.settings.test'
    if DJANGO_ENV == 'STAGING':
        return 'ssw_bms.settings.stage'
    return 'ssw_bms.settings.dev'


def main():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                          get_django_settings_module())

    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
