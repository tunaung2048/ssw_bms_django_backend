# Django specific notes

# User Model
1. use in model relationship like referencing ForeignKey
    ```
    from django.conf import settings

    User = settings.AUTH_USER_MODEL
    ```

2. use in authentication
    ```
    from django.contrib.auth import get_user_model

    User = get_user_model
    ```

3. use in other cases
    ```
    from django.contrib.auth.models import User
    ```
    