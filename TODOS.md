# TODO List


    [ ] Fix settings modules
        [x] django_settings
            - currently using environment based settings 
            - dev.py, prod.py, test.py, stage.py
            - third party settings are embeded inside respective files
            - prod settings are read from .env file located at project root
        [ ] drf_settings
        [ ] mptt_settings
        [ ] simplejwt_settings      
    [ ] Add signal to chain create user profile
        As soon as a user account is created, its related profile is created automatically using django signal.