# SSW BMS Django Backend API
A backend internal api for ssw bms using Django + Django Rest Framework


## Tech Stack
    - Web Framework
      - Django web framework
        - Django rest framework
    - Database Engine
      - PostgreSQL
    - Web Server
      - Nginx
      - Gunicorn